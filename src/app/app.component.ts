import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  postList = [new Post('Mon premier post', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis'),
  new Post('Mon deuxième post', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis'),
  new Post('Encore un post', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'),
  ];

  constructor() {
  }

}

export class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: Date;

  constructor(title: string, content: string) {
    this.title = title;
    this.content = content;
    this.created_at = new Date();
    this.loveIts = 0;
  }

  likePost(){
    this.loveIts++;
  }

  dislikePost(){
    this.loveIts--;
  }

  isLiked(){
    return this.loveIts > 0;
  }

  isDisliked(){
    return this.loveIts < 0;
  }

  getColor() {
    if(this.isLiked()) {
      return 'green';
    } else if(this.isDisliked()) {
      return 'red';
    } else {
      return 'black';
    }
  }

}